# About me

![](../images/avatar-photo.jpg)

Hello there! My name is Kevin Cummings, and I am a student in Lorain County Community College's Digital Fabrication program.



## My background

I live in Bay Village, Ohio and I went to Bay High School. 



### Project A

This is an image from an external site:

![This is the image caption](https://images.unsplash.com/photo-1512436991641-6745cdb1723f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ad25f4eb5444edddb0c5fb252a7f1dce&auto=format&fit=crop&w=900&q=80)

While this is an image from the assets/images folder. Never use absolute paths (starting with /) when linking local images, always relative.

![This is another caption](../images/sample-photo.jpg)
