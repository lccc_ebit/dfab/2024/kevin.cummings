# Molding and Casting Project

For the molding and casting project, I decided I was going to make a dog paw shape, as it would have rounded edges and would be interesting.

First, I modeled the dog paw in Fusion 360.

![Image](docs\projects\moldandcast\MACaspire.jpg)

Next, I added walls and a base to create a cavity that would result in a mold.

![Image](docs\projects\moldandcast\MACfusion2.jpg)

Now it was time to cut out the mold. The material was machinable wax, and the desktop shopbot was used.

![Image](docs\projects\moldandcast\MACaspire.jpg)

